#!/bin/sh
# Public domain

case $1 in
    "")
        echo "Using build.conf"
        . $(dirname "$0")/build.conf
        ;;
    *)
        docker_imgs="$@"
        ;;
esac

if [ -z "$docker_imgs" ] ; then
    echo "No docker imgs to build"
    exit 0
fi

#=========================================================

docker_login()
{
    if [ -z "$DOCKER_USERNAME" ] ; then
       echo "\$DOCKER_USERNAME has not been set"
        exit 1
    fi
    if [ -z "$DOCKER_PASSWORD" ] ; then
        echo "\$DOCKER_PASSWORD has not been set"
        exit 1
    fi
    printenv DOCKER_PASSWORD | docker login --username $DOCKER_USERNAME --password-stdin
    if [ $? -ne 0 ] ; then
        echo "Could not log into account"
        exit 1
    fi
}

#=========================================================

s390x_alpine()
{
    set -ex
    docker buildx create --name wbuilder
    docker buildx use wbuilder
    docker buildx inspect --bootstrap
    docker buildx build ${QUIET_OPT} --push --platform linux/s390x --tag wdlkmpx/gcc:s390x_alpine s390x_alpine
    docker buildx rm wbuilder
    set +x
}


s390x_debian()
{
    set -ex
    docker buildx create --name wbuilder
    docker buildx use wbuilder
    docker buildx inspect --bootstrap
    docker buildx build ${QUIET_OPT} --push --platform linux/s390x --tag wdlkmpx/gcc:s390x_debian s390x_debian
    docker buildx rm wbuilder
    set +x
}

#=========================================================
# build

docker_login

echo "+ docker buildx version"
docker buildx version

QUIET_OPT=''
# some CI may produce too verbose logs (1 MB+)
if [ "$CIRCLE_BRANCH" ] ; then
    QUIET_OPT='-q'
fi

for i in ${docker_imgs}
do
    case $i in
        "") continue ;;
        "#"*) echo "$i is disabled" ; continue ;;
    esac
    if [ ! -f "$i/Dockerfile" ] ; then
        echo "ERROR: $i/Dockerfile doesn't exist"
        exit 1
    fi
    echo
    echo "-----------------"
    echo $i
    echo "-----------------"
    $i
done
